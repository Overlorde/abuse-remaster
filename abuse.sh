#!/bin/bash
#xhost +local:root # autorisation pour l'execution de X11

sudo docker run -i -t --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --device /dev/snd \
    -e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native \
    -v ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native \
    -v ~/.config/pulse/cookie:/root/.config/pulse/cookie \
    --group-add $(getent group audio | cut -d: -f3) \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    abuse bash
    # shellcheck disable=SC2046
